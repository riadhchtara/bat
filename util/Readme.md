This subproject contains classes that define common functionality that is generally useful.

In particular it defines macros which are used by BAT-Core and which need to be compiled before we compile the core.