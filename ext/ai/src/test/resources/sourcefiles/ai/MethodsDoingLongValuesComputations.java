/* License (BSD Style License):
 * Copyright (c) 2009 - 2013
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package ai;

import java.lang.reflect.Array;

/**
 * Tests for long values for the the lz precise domain
 * @author Riadh Chtara
 */
@SuppressWarnings("all")
public class MethodsDoingLongValuesComputations {
	
    //
    // RETURNS

	public long cons() {
		return 1234567890123400l;
	}
	
	public long along() {
		return (long)(Math.random()*100);
	}
	
	public long neg() {
		long v = 1234567890123400l;
		return -v;
	}
	
	public long div() {
		long v1 = 1234567890123400l; 
		long v2 = 1234567890123456l;
		return v1 / v2;
	}
	
	public long sum() {
		long v1 = 1234567890123400l; 
		long v2 = 1234567890123456l;
		return v1 + v2;
	}

	public long and() {
		long v1 = 1234567890123400l; 
		long v2 = 1234567890123456l;
		return v1 & v2;
	}
	
	public long spand() {
		long v1 = 0; 
		long v2 = 1234567890123456l + (long)(Math.random()*100);
		return v1 & v2;
	}
	
	public long mul() {
		long v1 = 1234567890123400l; 
		long v2 = 1234567890123456l;
		return v1 * v2;
	}
	
	public long spmul() {
		long v1 = 0; 
		long v2 = 1234567890123456l + (long)(Math.random()*100);
		return v1 * v2;
	}

	public long or() {
		long v1 = 1234567890123400l; 
		long v2 = 1234567890123456l;
		return v1 | v2;
	}
	
	public long rem() {
		long v1 = 1234567890123400l; 
		long v2 = 1234567890123456l;
		return v1 % v2;
	}
	
	public long shl() {
		return 1234567890123400l << 1234567890123456l; 
	}

	public long shr() {
		return  1234567890123400l >> 1234567890123456l; 
	}

	public long dif() {
		long v1 = 1234567890123400l; 
		long v2 = 1234567890123456l;
		return v1 - v2;
	}
	
	public long ushr() {
		return 1234567890123400l >>> 1234567890123456l;
	}

	public long xor() {
		long v1 = 1234567890123400l; 
		long v2 = 1234567890123456l;
		return v1 ^ v2;
	}
	
	public long arithmetic(){
		long v1 = 1234567890123400l; 
		long v2 = 1234567890123456l;
		long v3 = 2l * v1 % 55l + 3l * (v2/5l) - 2l;
		return v3;
	}
	
	public long binary() {
		long v1 = 1234567890123400l; 
		long v2 = 1234567890123456l;
		long v3;
		v3 = 1l << 2;
		v3 = 2l >> 2;
		v3 = (v2 | v1) & (55l ^ 3l);
		return v3;
	}
	
	public long condition() {
		long v1 = 1234567890123400l; 
		long v2 = 1234567890123456l;
		long v3;
		if (v1>v2) v3 = 5l;
		else v3 = 2l;
		return v3;
	}
    //
    // CONVERSION
	
    public double l2d() {
    	long v = 1234567890128888l;
    	return (double) v;
    }
    
    public float l2f() {
    	long v = 1234567890128888l;
    	return (float) v;
    }
    
    public int l2i() {
    	long v = 1234567890128888l;
    	return (int) v;
    }
    
    
}
