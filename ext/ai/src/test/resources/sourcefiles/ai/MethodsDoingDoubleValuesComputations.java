/* License (BSD Style License):
 * Copyright (c) 2009 - 2013
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package ai;

import java.lang.reflect.Array;

/**
 * Tests for double values for the the lz precise domain
 * @author Riadh Chtara
 */
@SuppressWarnings("all")
public class MethodsDoingDoubleValuesComputations {
	
    //
    // RETURNS

	public double cons() {
		return 7.0d;
	}
	
	public double adouble() {
		return (double)(Math.random()*100);
	}
	
	public double adouble(double d) {
		return d;
	}
	
	public double adoubleconst(double d) {
		return d * 5.04d;
	}
	
	public double neg() {
		double v =  7.0d;
		return -v;
	}
	
	public double div() {
		double v1 = 7.0d; 
		double v2 = 8.0d;
		return v1 / v2;
	}
	
	public double sum() {
		double v1 = 7.0d; 
		double v2 = 8.0d;
		return v1 + v2;
	}

	public double dif() {
		double v1 = 7.0d; 
		double v2 = 8.0d;
		return v1 - v2;
	}
	
	public double mul() {
		double v1 = 7.0d; 
		double v2 = 8.0d;
		return v1 * v2;
	}
	
	public double spmul() {
		double v1 = 0; 
		double v2 = 1234567d + (double)(Math.random()*100);
		return v1 * v2;
	}

	public double rem() {
		double v1 = 7.0d; 
		double v2 = 8.0d;
		return v1 % v2;
	}
	
	public double arithmetic(){
		double v1 = 7.0d;
		double v2 = 8.0d;
		double v3 = 2.5d * v1 % 65.02d + 3.1d * (v2 / 10d) - 2d;
		return v3;
	}
	
	public double complexArithmetic() {
		double v1 = 7.0d;
		double v2 = 8.0d;
		double v3, v4;
		v3 = 5d * v1 % 5.02d + 3.1d * (v2 / 1.01d) - 2d;
		v4 = 2.5d * v1 % 65.02f + 3.1d * (v2 / 10d) - 2d;
		return v4 + v3;
	}
	
	public double condition() {
		double v1 = 7.0d;
		double v2 = 8.0d;
		double v3;
		if (v1>v2) v3 = 5d;
		else v3 = 2d;
		return v3;
	}
	
    //
    // CONVERSION
	
    public int d2i() {
    	double v = 8.1f;
    	return (int) v;
    }
    
    public float d2f() {
    	double v = 8.1d;
    	return (float) v;
    }
    
    public long d2l() {
    	double v = 8.1f;
    	return (long) v;
    }
}
