/* License (BSD Style License):
 * Copyright (c) 2009 - 2013
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package ai;

import java.lang.reflect.Array;

/**
 * Tests for int values for the the lz precise domain
 * @author Riadh Chtara
 */
@SuppressWarnings("all")
public class MethodsDoingIntegerValuesComputations {
	
    //
    // RETURNS

	public int cons() {
		return 400;
	}
	
	public int aninteger() {
		return (int)(Math.random()*100);
	}
	
	public int neg() {
		int v = 400;
		return -v;
	}
	
	public int div() {
		int v1 = 400; 
		int v2 = 600;
		return v1 / v2;
	}
	
	public int sum() {
		int v1 = 400; 
		int v2 = 600;
		return v1 + v2;
	}

	public int and() {
		int v1 = 400; 
		int v2 = 600;
		return v1 & v2;
	}
	
	public int spand() {
		int v1 = 0; 
		int v2 = 600 + (int)(Math.random()*100);
		return v1 & v2;
	}
	
	public int mul() {
		int v1 = 400; 
		int v2 = 600;
		return v1 * v2;
	}
	
	public int spmul() {
		int v1 = 0; 
		int v2 = 600 + (int)(Math.random()*100);
		return v1 * v2;
	}

	public int or() {
		int v1 = 400; 
		int v2 = 600;
		return v1 | v2;
	}
	
	public int rem() {
		int v1 = 400; 
		int v2 = 600;
		return v1 % v2;
	}
	
	public int shl() {
		return 400 << 600; 
	}

	public int shr() {
		return  400 >> 600; 
	}

	public int dif() {
		int v1 = 400; 
		int v2 = 600;
		return v1 - v2;
	}
	
	public int ushr() {
		return 400 >>> 600;
	}

	public int xor() {
		int v1 = 400; 
		int v2 = 600;
		return v1 ^ v2;
	}
	
	public int arithmetic(){
		int v1 = 400; 
		int v2 = 600;
		int v3 = 2 * v1 % 55 + 3 * (v2/5) - 2;
		return v3;
	}
	
	public int binary() {
		int v1 = 400; 
		int v2 = 600;
		int v3;
		v3 = 1 << 2;
		v3 = 2 >> 2;
		v3 = (v2 | v1) & (55 ^ 3);
		return v3;
	}
	
	public int condition() {
		int v1 = 400; 
		int v2 = 600;
		int v3;
		if (v1>v2) v3 = 5;
		else v3 = 2;
		return v3;
	}
    //
    // CONVERSION
	
    public boolean i2b() {
    	return true;
    }
    
    public char i2c() {
    	int v = 12345;
    	return (char) v;
    }
    
    public double i2d() {
    	int v = 12345;
    	return (double) v;
    }
    
    public float i2f() {
    	int v = 12345;
    	return (float) v;
    }
    
    public long i2l() {
    	int v = 12345;
    	return (long) v;
    }
}
