/* License (BSD Style License):
 * Copyright (c) 2009 - 2013
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package ai;

import java.lang.reflect.Array;

/**
 * Tests for float values for the the lz precise domain
 * @author Riadh Chtara
 */
@SuppressWarnings("all")
public class MethodsDoingFloatValuesComputations {
	
    //
    // RETURNS

	public float cons() {
		return 7.0f;
	}
	
	public float afloat() {
		return (float)(Math.random()*100);
	}
	
	public float neg() {
		float v =  7.0f;
		return -v;
	}
	
	public float div() {
		float v1 = 7.0f; 
		float v2 = 8.0f;
		return v1 / v2;
	}
	
	public float sum() {
		float v1 = 7.0f; 
		float v2 = 8.0f;
		return v1 + v2;
	}

	public float dif() {
		float v1 = 7.0f; 
		float v2 = 8.0f;
		return v1 - v2;
	}
	
	public float mul() {
		float v1 = 7.0f; 
		float v2 = 8.0f;
		return v1 * v2;
	}
	
	public float spmul() {
		float v1 = 0; 
		float v2 = 1234567f + (float)(Math.random()*100);
		return v1 * v2;
	}
	public float rem() {
		float v1 = 7.0f; 
		float v2 = 8.0f;
		return v1 % v2;
	}
	
	public float arithmetic(){
		float v1 = 7.0f;
		float v2 = 8.0f;
		float v3 = 2.5f * v1 % 65.02f + 3.1f * (v2 / 10f) - 2f;
		return v3;
	}

	public float complexArithmetic() {
		float v1 = 7.0f;
		float v2 = 8.0f;
		float v3, v4;
		v3 = 5f * v1 % 5.02f + 3.1f * (v2/1.01f) - 2f;
		v4 = 2.5f * v1 % 65.02f + 3.1f * (v2 / 10f) - 2f;
		return v4 + v3;
	}
	
	public float condition() {
		float v1 = 7.0f;
		float v2 = 8.0f;
		float v3;
		if (v1>v2) v3 = 5f;
		else v3 = 2f;
		return v3;
	}
    //
    // CONVERSION
	
    public double f2d() {
    	float v = 8.1f;
    	return (double) v;
    }
    
    public int f2i() {
    	float v = 8.1f;
    	return (int) v;
    }
    
    public long f2l() {
    	float v = 8.1f;
    	return (long) v;
    }
}
