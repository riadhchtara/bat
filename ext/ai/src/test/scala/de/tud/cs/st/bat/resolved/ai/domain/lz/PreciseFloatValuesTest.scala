/* License (BSD Style License):
 * Copyright (c) 2009 - 2013
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tud.cs.st
package bat
package resolved
package ai
package domain
package lz

import de.tud.cs.st.util.{ Answer, Yes, No, Unknown }

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FlatSpec
import org.scalatest.matchers.ShouldMatchers
import org.scalatest.concurrent.TimeLimitedTests
import org.scalatest.time._
import org.scalatest.BeforeAndAfterAll
import org.scalatest.ParallelTestExecution

import ObjectType.ArithmeticException

/**
 * This test(suite) checks if PreciseFloatValues is working fine
 *
 * @author Riadh Chtara
 */
@RunWith(classOf[JUnitRunner])
class PreciseFloatValuesTest
        extends FlatSpec
        with ShouldMatchers
        with ParallelTestExecution {

    val domain = new PreciseConfigurableDomain("PreciseFloatValuesTest")
    import domain._

    //
    // TESTS
    //

    behavior of "the precise float values domain"
      
    //
    // QUESTION'S ABOUT VALUES
    //
    
    it should ("be able to check if two float values are equal") in {
    	val v1 = FloatValue(-1, 7.0f)
    	val v2 = FloatValue(-1, 7.0f)
    	val v3 = FloatValue(-1, 8.0f) 
    	v1.equals(v2) should be(true)
    	v1.equals(v3) should be(false)
    }
   
    
    //
    // RELATIONAL OPERATORS
    //
        
    it should ("be able to compare two float values (great)") in {
    	val v1 = FloatValue(-1, 7.0f)
    	val v2 = FloatValue(-1, 8.0f)
    	fcmpg(-1, v1, v2) should be(IntegerValue(-1, 1))
    	fcmpg(-1, v1, v1) should be(IntegerValue(-1, 0))
    	fcmpg(-1, v2, v1) should be(IntegerValue(-1, -1))
    	
    	val z1 = FloatValue(-1, 0.0f)
    	val z2 = FloatValue(-1, -0.0f)
    	
    	fcmpg(-1, z1, z2) should be(IntegerValue(-1, 0))
    	fcmpg(-1, z2, z1) should be(IntegerValue(-1, 0))
    	
    	val nan = FloatValue(-1, Float.NaN)
    	fcmpg(-1, v1, nan) should be(IntegerValue(-1, 1))
    	fcmpg(-1, nan, v1) should be(IntegerValue(-1, 1))
    }
    
    it should ("be able to compare two float values (less)") in {
    	val v1 = FloatValue(-1, 7.0f)
    	val v2 = FloatValue(-1, 8.0f)
    	fcmpl(-1, v1, v2) should be(IntegerValue(-1, -1))
    	fcmpl(-1, v1, v1) should be(IntegerValue(-1, 0))
    	fcmpl(-1, v2, v1) should be(IntegerValue(-1, 1))
    	
    	val z1 = FloatValue(-1, 0.0f)
    	val z2 = FloatValue(-1, -0.0f)
    	
    	fcmpl(-1, z1, z2) should be(IntegerValue(-1, 0))
    	fcmpl(-1, z2, z1) should be(IntegerValue(-1, 0))
    	
    	val nan = FloatValue(-1, Float.NaN)
    	fcmpl(-1, v1, nan) should be(IntegerValue(-1, 1))
    	fcmpl(-1, nan, v1) should be(IntegerValue(-1, 1))
    }
    
    //
    // UNARY EXPRESSIONS
    //
    
    it should ("be able to the calculate the neg of a float value") in {
    	val v1 = FloatValue(-1, 7.0f)
    	val v2 = fneg(-1, fneg(-1, v1))
    	v1.equals(v2) should be(true)
    	FloatValue(-1, Float.MinValue) should be(FloatValue(-1, Float.MinValue))
    }
    
    //
    // BINARY EXPRESSIONS
    //
    //TODO l to d
    it should ("be able to the calculate the result of the add of two float values") in {
    	val v1 = FloatValue(-1, 7.0f)
    	val v2 = FloatValue(-1, 6.0f)
    	fadd(-1, v1, v2) should be(FloatValue(-1, 7.0f + 6.0f))
    }
    
    it should ("be able to the calculate the result of the div of two float values") in {
    	val v1 = FloatValue(-1, 7.0f)
    	val v2 = FloatValue(-1, 6.0f)

    	//todo comment new test 
    	fdiv(-1, v1, v2) should be(FloatValue(-1, 7.0f/ 6.0f))
    }
        
    it should ("be able to the calculate the result of the mul of two float values") in {
    	val v1 = FloatValue(-1, 7.0f)
    	val v2 = FloatValue(-1, 6.0f)
    	fmul(-1, v1, v2) should be(FloatValue(-1, 7.0f * 6.0f))
    }
    
    it should ("know that that the result of the mul of zero and a float value is zero") in {
    	val zero = FloatValue(-1, 0.0f)
    	val v = AFloatValue
    	fmul(-1, v, zero) should be (FloatValue(-1, 0.0f))
    	fmul(-1, zero, v) should be (FloatValue(-1, 0.0f))
    }
             
    it should ("be able to the calculate the result of the rem of two float values") in {
    	val v1 = FloatValue(-1, 7.0f)
    	val v2 = FloatValue(-1, 6.0f)
    	frem(-1, v1, v2) should be(FloatValue(-1, 7.0f % 6.0f))    	
    }

    it should ("be able to the calculate the result of the sub of two float values") in {
    	val v1 = FloatValue(-1, 7.0f)
    	val v2 = FloatValue(-1, 6.0f)
    	fsub(-1, v1, v2) should be(FloatValue(-1, 7.0f - 6.0f))
    }
}

