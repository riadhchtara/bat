/* License (BSD Style License):
 * Copyright (c) 2009 - 2013
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tud.cs.st
package bat
package resolved
package ai
package domain
package lz

import reader.Java7Framework.ClassFiles
import domain._
import domain.lz._

import de.tud.cs.st.util.{ Answer, Yes, No, Unknown }

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FlatSpec
import org.scalatest.BeforeAndAfterAll
import org.scalatest.ParallelTestExecution
import org.scalatest.matchers.ShouldMatchers
import org.scalatest.matchers.MatchResult

/**
 * Tests for float values for the the lz precise domain
 *
 * @author Riadh Chtara
 */
@RunWith(classOf[JUnitRunner])
class MethodsDoingFloatValuesComputationsTest
        extends FlatSpec
        with ShouldMatchers /*with BeforeAndAfterAll */
        with ParallelTestExecution {
    
    private[this] val IrrelevantPC = Int.MinValue

    import debug.XHTML.dumpOnFailureDuringValidation
    import MethodsDoingFloatValuesComputationsTest._

    behavior of "the precise lz domain for float"

    //
    // RETURNS
    

    it should "be able to analyze a method that returns a fixed float value" in {
        val domain = new RecordingDomain; import domain._;
        val method = classFile.methods.find(_.name == "cons").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(FloatValue(-1, 7.0f)))
    }
     
    it should "be able to analyze a method that returns a unknown float value" in {
        val domain = new RecordingDomain; import domain._;
        val method = classFile.methods.find(_.name == "afloat").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(FloatValue(-1)))
    }
    
    it should "be able to analyze a method that returns the neg of float value" in {
        val domain = new RecordingDomain; import domain._;
        val method = classFile.methods.find(_.name == "neg").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(FloatValue(-1, -7.0f)))
    }
    
    it should "be able to analyze a method that divides two floats" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "div").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(FloatValue(-1, 7.0f / 8.0f)))
    }

    it should "be able to analyze a method that calculates the sum of two floats" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "sum").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(FloatValue(-1, 7.0f + 8.0f)))
    }
 
    it should "be able to analyze a method that substracts two floats" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "dif").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(FloatValue(-1, 7.0f - 8.0f)))
    }
    
    it should "be able to analyze a method that multiply two floats" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "mul").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(FloatValue(-1,  7.0f * 8.0f)))
    }
    
    it should "be able to analyze a method that calculates the mul of zero and a float value" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "spmul").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(FloatValue(-1, 0.0f)))
    }
    
    it should "be able to analyze a method that calculates the rem of two floats" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "rem").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(FloatValue(-1, 7.0f % 8.0f)))
    }
    
    it should "be able to analyze a method doing an arithmetic computation" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "arithmetic").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(FloatValue(-1, 17.98f)))
    }
    
    it should "be able to analyze a method doing a complex arithmetic computation" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "complexArithmetic").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(FloatValue(-1, 45.414455f)))
    }
    
    it should "be able to analyze a method having a condition" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "condition").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(FloatValue(-1, 2f)))
    }
    
    //
    // CONVERSION
 
    it should "be able to analyze a method that converts a float to double" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "f2d").get
        /*val result =*/ BaseAI(classFile, method, domain)

        val v = 8.1f;
        domain.returnedValue should be(Some(DoubleValue(-1, v.asInstanceOf[Double])))
    }

    it should "be able to analyze a method that converts a float to int" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "f2i").get
        /*val result =*/ BaseAI(classFile, method, domain)

        val v = 8.1f;
        domain.returnedValue should be(Some(IntegerValue(-1, v.asInstanceOf[Int])))
    }
    
    it should "be able to analyze a method that converts a float to long" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "f2l").get
        /*val result =*/ BaseAI(classFile, method, domain)

        val v = 8.1f;
        domain.returnedValue should be(Some(LongValue(-1, v.asInstanceOf[Long])))
    }
}

private object MethodsDoingFloatValuesComputationsTest {

    class RecordingDomain extends domain.lz.PreciseDomain[String]
            with IgnoreSynchronization
            with IgnoreThrownExceptions {
        def identifier = "SimpleRecordingDomain"
        var returnedValue: Option[DomainValue] = _
        override def areturn(pc: Int, value: DomainValue) { returnedValue = Some(value) }
        override def dreturn(pc: Int, value: DomainValue) { returnedValue = Some(value) }
        override def freturn(pc: Int, value: DomainValue) { returnedValue = Some(value) }
        override def ireturn(pc: Int, value: DomainValue) { returnedValue = Some(value) }
        override def lreturn(pc: Int, value: DomainValue) { returnedValue = Some(value) }
        override def returnVoid(pc: Int) { returnedValue = None }
    }

    val classFile =
        ClassFiles(TestSupport.locateTestResources("classfiles/ai.jar", "ext/ai")).map(_._1).
            find(_.thisType.fqn == "ai/MethodsDoingFloatValuesComputations").get
}