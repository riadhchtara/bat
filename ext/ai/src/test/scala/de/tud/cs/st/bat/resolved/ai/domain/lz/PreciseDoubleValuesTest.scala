/* License (BSD Style License):
 * Copyright (c) 2009 - 2013
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tud.cs.st
package bat
package resolved
package ai
package domain
package lz

import de.tud.cs.st.util.{ Answer, Yes, No, Unknown }

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FlatSpec
import org.scalatest.matchers.ShouldMatchers
import org.scalatest.concurrent.TimeLimitedTests
import org.scalatest.time._
import org.scalatest.BeforeAndAfterAll
import org.scalatest.ParallelTestExecution

import ObjectType.ArithmeticException

/**
 * This test(suite) checks if PreciseDoubleValues is working fine
 *
 * @author Riadh Chtara
 */
@RunWith(classOf[JUnitRunner])
class PreciseDoubleValuesTest
        extends FlatSpec
        with ShouldMatchers
        with ParallelTestExecution {

    val domain = new PreciseConfigurableDomain("PreciseDoubleValuesTest")
    import domain._

    //
    // TESTS
    //

    behavior of "the precise double values domain"
      
    //
    // QUESTION'S ABOUT VALUES
    //
    
    it should ("be able to check if two double values are equal") in {
    	val v1 = DoubleValue(-1, 7.0d)
    	val v2 = DoubleValue(-1, 7.0d)
    	val v3 = DoubleValue(-1, 8.0d) 
    	v1.equals(v2) should be(true)
    	v1.equals(v3) should be(false)
    }
    
    //
    // RELATIONAL OPERATORS
    //
    
    it should ("be able to compare two double values (great)") in {
    	val v1 = DoubleValue(-1, 7.0d)
    	val v2 = DoubleValue(-1, 8.0d)
    	dcmpg(-1, v1, v2) should be(IntegerValue(-1, 1))
    	dcmpg(-1, v1, v1) should be(IntegerValue(-1, 0))
    	dcmpg(-1, v2, v1) should be(IntegerValue(-1, -1))
    	
    	val z1 = DoubleValue(-1, 0.0d)
    	val z2 = DoubleValue(-1, -0.0d)
    	
    	dcmpg(-1, z1, z2) should be(IntegerValue(-1, 0))
    	dcmpg(-1, z2, z1) should be(IntegerValue(-1, 0))
    	
    	val nan = DoubleValue(-1, Double.NaN)
    	dcmpg(-1, v1, nan) should be(IntegerValue(-1, 1))
    	dcmpg(-1, nan, v1) should be(IntegerValue(-1, 1))
    }
    
    it should ("be able to compare two double values (less)") in {
    	val v1 = DoubleValue(-1, 7.0d)
    	val v2 = DoubleValue(-1, 8.0d)
    	dcmpl(-1, v1, v2) should be(IntegerValue(-1, -1))
    	dcmpl(-1, v1, v1) should be(IntegerValue(-1, 0))
    	dcmpl(-1, v2, v1) should be(IntegerValue(-1, 1))
    	
    	val z1 = DoubleValue(-1, 0.0d)
    	val z2 = DoubleValue(-1, -0.0d)
    	
    	dcmpl(-1, z1, z2) should be(IntegerValue(-1, 0))
    	dcmpl(-1, z2, z1) should be(IntegerValue(-1, 0))
    	
    	val nan = DoubleValue(-1, Double.NaN)
    	dcmpl(-1, v1, nan) should be(IntegerValue(-1, 1))
    	dcmpl(-1, nan, v1) should be(IntegerValue(-1, 1))
    }
    
    //
    // UNARY EXPRESSIONS
    //
    
    it should ("be able to the calculate the neg of a double value") in {
    	val v1 = DoubleValue(-1, 2.0d)
    	val v2 = dneg(-1, dneg(-1, v1))
    	v1.equals(v2) should be(true)
        DoubleValue(-1, Double.MinValue) should be(DoubleValue(-1, Double.MinValue))
    }
    
    //
    // BINARY EXPRESSIONS
    //
    //TODO l to d
    it should ("be able to the calculate the result of the add of two double values") in {
    	val v1 = DoubleValue(-1, 7.0d)
    	val v2 = DoubleValue(-1, 6.0d)
    	dadd(-1, v1, v2) should be(DoubleValue(-1, 7.0d + 6.0d))
    }
    
    it should ("be able to the calculate the result of the div of two double values") in {
    	val v1 = DoubleValue(-1, 7.0d)
    	val v2 = DoubleValue(-1, 6.0d)
    	ddiv(-1, v1, v2)
    	//todo comment new test 
    	ddiv(-1, v1, v2) should be(DoubleValue(-1, 7.0d/ 6.0d))
    }
        
    it should ("be able to the calculate the result of the mul of two double values") in {
    	val v1 = DoubleValue(-1, 7.0d)
    	val v2 = DoubleValue(-1, 6.0d)
    	dmul(-1, v1, v2) should be(DoubleValue(-1, 7.0d * 6.0d))
    }
    
    it should ("know that that the result of the mul of zero and a double value is zero") in {
    	val zero = DoubleValue(-1, 0.0f)
    	val v = ADoubleValue()
    	dmul(-1, v, zero) should be (DoubleValue(-1, 0.0d))
    	dmul(-1, zero, v) should be (DoubleValue(-1, 0.0d))
    }
             
    it should ("be able to the calculate the result of the rem of two double values") in {
    	val v1 = DoubleValue(-1, 7.0d)
    	val v2 = DoubleValue(-1, 6.0d)
    	drem(-1, v1, v2) should be(DoubleValue(-1, 7.0d % 6.0d))
  	
    }

    it should ("be able to the calculate the result of the sub of two double values") in {
    	val v1 = DoubleValue(-1, 7.0d)
    	val v2 = DoubleValue(-1, 6.0d)
    	dsub(-1, v1, v2) should be(DoubleValue(-1, 7.0d - 6.0d))
    }
}

