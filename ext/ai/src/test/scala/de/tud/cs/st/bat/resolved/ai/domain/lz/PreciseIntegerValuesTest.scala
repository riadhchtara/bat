/* License (BSD Style License):
 * Copyright (c) 2009 - 2013
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tud.cs.st
package bat
package resolved
package ai
package domain
package lz

import de.tud.cs.st.util.{ Answer, Yes, No, Unknown }

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FlatSpec
import org.scalatest.matchers.ShouldMatchers
import org.scalatest.concurrent.TimeLimitedTests
import org.scalatest.time._
import org.scalatest.BeforeAndAfterAll
import org.scalatest.ParallelTestExecution

import ObjectType.ArithmeticException

/**
 * This test(suite) checks if PreciseIntegerValues is working fine
 *
 * @author Riadh Chtara
 */
@RunWith(classOf[JUnitRunner])
class PreciseIntegerValuesTest
        extends FlatSpec
        with ShouldMatchers
        with ParallelTestExecution {

    val domain = new PreciseConfigurableDomain("PreciseIntegerValuesTest")
    import domain._

    //
    // TESTS
    //
    behavior of "the precise integer values domain"
      
    //
    // QUESTION'S ABOUT VALUES
    //
    
    it should ("be able to check if two integer values are equal") in {
    	val v1 = IntegerValue(-1, 123)
    	val v2 = IntegerValue(-1, 123)
    	val v3 = IntegerValue(-1, 1234) 
    	v1.equals(v2) should be(true)
    	v1.equals(v3) should be(false)
    }
    
    it should ("be able to check if an integer value is less than another value") in {
    	val v1 = IntegerValue(-1, 7)
    	val v2 = IntegerValue(-1, 8)
    	isLessThan(v1, v2) should be(Answer(true))
    	isLessThan(v1, v1) should be(Answer(false))	
    	isLessThan(v2, v1) should be(Answer(false))	
    }
    
    it should ("be able to check if an nteger value is less than or equal another value") in {
    	val v1 = IntegerValue(-1, 7)
    	val v2 = IntegerValue(-1, 8)
    	isLessThanOrEqualTo(v1, v2) should be(Answer(true))
    	isLessThanOrEqualTo(v1, v1) should be(Answer(true))	
    	isLessThanOrEqualTo(v2, v1) should be(Answer(false))	
    }

    //
    // UNARY EXPRESSIONS
    //
    
    it should ("be able to the calculate the neg of a integer value") in {
    	val v1 = IntegerValue(-1, 4)
    	val v2 = ineg(-1, ineg(-1, v1))
    	v1.equals(v2) should be(true)
    	IntegerValue(-1, Int.MinValue) should be(IntegerValue(-1, Int.MinValue))
    }
    
    //
    // BINARY EXPRESSIONS
    //
    
    it should ("be able to the calculate the result of the div of two integer values") in {
    	val v1 = IntegerValue(-1, 1234)
    	val v2 = IntegerValue(-1, 123)
    	idiv(-1, v1, v2) should be(ComputedValue(IntegerValue(-1, 1234 / 123)))
    	idiv(-1, v1, IntegerValue(-1, 0)) should be (ThrowsException(InitializedObjectValue(-1, ArithmeticException)))
    }
    
    it should ("be able to the calculate the result of the add of two integer values") in {
    	val v1 = IntegerValue(-1, 12340)
    	val v2 = IntegerValue(-1, 12345)
    	iadd(-1, v1, v2) should be(IntegerValue(-1,  12340 + 12345))
    }
    
    it should ("be able to the calculate the result of the and of two integer values") in {
    	val v1 = IntegerValue(-1, 12340)
    	val v2 = IntegerValue(-1, 12345)
    	iand(-1, v1, v2) should be(IntegerValue(-1, 12340 & 12345))
    }
    
    it should ("know that that the result of the and of zero and a integer value is zero") in {
    	val zero = IntegerValue(-1, 0)
    	val v = AnIntegerValue()
    	iand(-1, v, zero) should be (IntegerValue(-1, 0))
    	iand(-1, zero, v) should be (IntegerValue(-1, 0))
    }
        
    it should ("be able to the calculate the result of the mul of two integer values") in {
    	val v1 = IntegerValue(-1, 12340)
    	val v2 = IntegerValue(-1, 12345)
    	imul(-1, v1, v2) should be(IntegerValue(-1, 12340 * 12345))
    }
    
    it should ("know that that the result of the mul of zero and a integer value is zero") in {
    	val zero = IntegerValue(-1, 0)
    	val v = AnIntegerValue()
    	imul(-1, v, zero) should be (IntegerValue(-1, 0))
    	imul(-1, zero, v) should be (IntegerValue(-1, 0))
    }
            
    it should ("be able to the calculate the result of the or of two integer values") in {
    	val v1 = IntegerValue(-1,  12340)
    	val v2 = IntegerValue(-1, 12345)
    	ior(-1, v1, v2) should be(IntegerValue(-1,  12340 | 12345))
    }
             
    it should ("be able to the calculate the result of the rem of two integer values") in {
    	val v1 = IntegerValue(-1, 12340)
    	val v2 = IntegerValue(-1, 12345)
    	irem(-1, v1, v2) should be(ComputedValue(IntegerValue(-1, 12340 % 12345)))
    	
        //TODO implement the exception 
    	//lrem(-1, v1, IntegerValue(-1, 0)) should be (ThrowsException(InitializedObject(-1, ArithmeticException)))
    	
    }
                
    it should ("be able to the calculate the result of the shl of two integer values") in {
    	val v1 = IntegerValue(-1, 12340)
    	val v2 = IntegerValue(-1, 12345)
    	ishl(-1, v1, v2) should be(IntegerValue(-1, 12340 << 12345))
    }
                
    it should ("be able to the calculate the result of the or of shr integer values") in {
    	val v1 = IntegerValue(-1,  12340)
    	val v2 = IntegerValue(-1, 12345)
    	ishr(-1, v1, v2) should be(IntegerValue(-1,  12340 >> 12345))
    }
                
    it should ("be able to the calculate the result of the sub of two integer values") in {
    	val v1 = IntegerValue(-1, 12340)
    	val v2 = IntegerValue(-1, 12345)
    	isub(-1, v1, v2) should be(IntegerValue(-1, 12340 - 12345))
    }
    
    it should ("be able to the calculate the result of the ushr of two integer values") in {
    	val v1 = IntegerValue(-1, 12340)
    	val v2 = IntegerValue(-1, 12345)
    	iushr(-1, v1, v2) should be(IntegerValue(-1, 12340 >>> 12345))
    }

    it should ("be able to the calculate the result of the xor of two integer values") in {
    	val v1 = IntegerValue(-1,  12340)
    	val v2 = IntegerValue(-1, 12345)
    	ixor(-1, v1, v2) should be(IntegerValue(-1, 12340 ^ 12345))
    }
    
    it should ("be able to the calculate the result of the inc of two integer values") in {
    	val v1 = IntegerValue(-1,  12340)
    	iinc(-1, v1, 12345) should be(IntegerValue(-1, 12340 + 12345))
    }
}

