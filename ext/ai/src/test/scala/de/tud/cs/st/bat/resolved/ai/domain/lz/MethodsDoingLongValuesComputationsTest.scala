/* License (BSD Style License):
 * Copyright (c) 2009 - 2013
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tud.cs.st
package bat
package resolved
package ai
package domain
package lz

import reader.Java7Framework.ClassFiles
import domain._
import domain.lz._

import de.tud.cs.st.util.{ Answer, Yes, No, Unknown }

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FlatSpec
import org.scalatest.BeforeAndAfterAll
import org.scalatest.ParallelTestExecution
import org.scalatest.matchers.ShouldMatchers
import org.scalatest.matchers.MatchResult

/**
 * Tests for long values for the the lz precise domain
 *
 * @author Riadh Chtara
 */
@RunWith(classOf[JUnitRunner])
class MethodsDoingLongValuesComputationsTest
        extends FlatSpec
        with ShouldMatchers /*with BeforeAndAfterAll */
        with ParallelTestExecution {
    
    private[this] val IrrelevantPC = Int.MinValue

    import debug.XHTML.dumpOnFailureDuringValidation
    import MethodsDoingLongValuesComputationsTest._

    behavior of "the prcise lz domain with long"

    //
    // RETURNS
    

    it should "be able to analyze a method that returns a fixed long value" in {
        val domain = new RecordingDomain; import domain._;
        val method = classFile.methods.find(_.name == "cons").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(LongValue(-1, 1234567890123400l)))
    }
    
    it should "be able to analyze a method that returns a unknown long value" in {
        val domain = new RecordingDomain; import domain._;
        val method = classFile.methods.find(_.name == "along").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(ALongValue()))
    }
    
    it should "be able to analyze a method that returns the neg of long value" in {
        val domain = new RecordingDomain; import domain._;
        val method = classFile.methods.find(_.name == "neg").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(LongValue(-1, -1234567890123400l)))
    }
    
    it should "be able to analyze a method that calcultes the div of two longs" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "div").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(LongValue(-1, 1234567890123400l / 1234567890123456l)))
    }

    it should "be able to analyze a method that calcultes the sum of two longs" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "sum").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(LongValue(-1, 1234567890123400l + 1234567890123456l)))
    }
    
    it should "be able to analyze a method that calcultes the and of two longs" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "and").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(LongValue(-1, 1234567890123400l & 1234567890123456l)))
    }
    
    it should "be able to analyze a method that calcultes the and of zero and a long value" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "spand").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(LongValue(-1, 0l)))
    }

    it should "be able to analyze a method that calcultes the mul of two longs" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "mul").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(LongValue(-1, 1234567890123400l * 1234567890123456l)))
    }
    
    it should "be able to analyze a method that calcultes the mul of zero and a long value" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "spmul").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(LongValue(-1, 0l)))
    }
    
    it should "be able to analyze a method that calcultes the or of two longs" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "or").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(LongValue(-1, 1234567890123400l | 1234567890123456l)))
    }
    
   it should "be able to analyze a method that calcultes the rem of two longs" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "rem").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(LongValue(-1, 1234567890123400l % 1234567890123456l)))
    }

    it should "be able to analyze a method that calcultes the shl of two longs" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "shl").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(LongValue(-1, 1234567890123400l << 1234567890123456l)))
    }
    
    it should "be able to analyze a method that calcultes the shr of two longs" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "shr").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(LongValue(-1, 1234567890123400l >> 1234567890123456l)))
    }
 
    it should "be able to analyze a method that calcultes the dif of two longs" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "dif").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(LongValue(-1, 1234567890123400l - 1234567890123456l)))
    }

    it should "be able to analyze a method that calcultes the ushr of two longs" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "ushr").get
        /*val result =*/ BaseAI(classFile, method, domain)
        //todo: fix this to get Some(LongValue(-1, 1234567890123400l >>> 1234567890123456l))
        domain.returnedValue should be(Some(ALongValue()))
    }
    
    it should "be able to analyze a method that calcultes the xor of two longs" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "xor").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(LongValue(-1, 1234567890123400l ^ 1234567890123456l)))
    }
    
    it should "be able to analyze a method doing an arithmetic computation" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "arithmetic").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(LongValue(-1, 740740734074076l)))
    }
    
    it should "be able to analyze a method doing a binary computation" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "binary").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(LongValue(-1, 0l)))
    }
    
    it should "be able to analyze a method having a condition" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "condition").get
        /*val result =*/ BaseAI(classFile, method, domain)

        domain.returnedValue should be(Some(LongValue(-1, 2l)))
    }
       
    //
    // CONVERSION
 
    it should "be able to analyze a method that converts a long to double" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "l2d").get
        /*val result =*/ BaseAI(classFile, method, domain)

        val v = 1234567890128888l;
        domain.returnedValue should be(Some(DoubleValue(-1, v.asInstanceOf[Double])))
    }

    it should "be able to analyze a method that converts a long to float" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "l2f").get
        /*val result =*/ BaseAI(classFile, method, domain)

        val v = 1234567890128888l;
        domain.returnedValue should be(Some(FloatValue(-1, v.asInstanceOf[Float])))
    }
    
    it should "be able to analyze a method that converts a long to int" in {
        val domain = new RecordingDomain; import domain._
        val method = classFile.methods.find(_.name == "l2i").get
        /*val result =*/ BaseAI(classFile, method, domain)

        val v = 1234567890128888l;
        domain.returnedValue should be(Some(IntegerValue(-1, v.asInstanceOf[Int])))
    }
}

private object MethodsDoingLongValuesComputationsTest {

    class RecordingDomain extends domain.lz.PreciseDomain[String]
            with IgnoreSynchronization
            with IgnoreThrownExceptions {
        def identifier = "SimpleRecordingDomain"
        var returnedValue: Option[DomainValue] = _
        override def areturn(pc: Int, value: DomainValue) { returnedValue = Some(value) }
        override def dreturn(pc: Int, value: DomainValue) { returnedValue = Some(value) }
        override def freturn(pc: Int, value: DomainValue) { returnedValue = Some(value) }
        override def ireturn(pc: Int, value: DomainValue) { returnedValue = Some(value) }
        override def lreturn(pc: Int, value: DomainValue) { returnedValue = Some(value) }
        override def returnVoid(pc: Int) { returnedValue = None }
    }

    val classFile =
        ClassFiles(TestSupport.locateTestResources("classfiles/ai.jar", "ext/ai")).map(_._1).
            find(_.thisType.fqn == "ai/MethodsDoingLongValuesComputations").get
}