/* License (BSD Style License):
 * Copyright (c) 2009 - 2013
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tud.cs.st
package bat
package resolved
package ai
package domain
package lz

import de.tud.cs.st.util.{ Answer, Yes, No, Unknown }

/**
 * Domain to track long values at a configurable level of precision.
 *
 * @author Riadh Chtara
 * @author Michael Eichberg
 */
trait LongValues[+I] extends l0.DefaultLongValuesBinding[I] {
    domain: Configuration with IntegerValuesComparison with ClassHierarchy ⇒

    /**
     * Represents a specific, but unknown long value.
     */
    case class ALongVal()
            extends super.ALongValue {
    }

    /**
     * Represents a concrete long value.
     */
    case class LongVal(override val value: Long)
            extends super.LongValue(value) {
        override def doJoin(pc: PC, other: DomainValue): Update[DomainValue] =
            other match {
                case v: LongVal ⇒
                    if (v.value == this.value) {
                        NoUpdate
                    } else {
                        StructuralUpdate(other)
                    }
                case v: ALongVal ⇒ StructuralUpdate(other)
            }
        override def toString: String = "LongValue(value="+value+")"
    }

    //
    // QUESTION'S ABOUT VALUES
    //

    override def withLongValueOrElse[T](value: DomainValue)(f: Long ⇒ T)(orElse: ⇒ T): T =
        value match {
            case value: LongVal ⇒ f(value.value)
            case _              ⇒ orElse
        }

}



