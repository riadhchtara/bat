/* License (BSD Style License):
 * Copyright (c) 2009 - 2013
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tud.cs.st
package bat
package resolved
package ai
package domain
package lz

import de.tud.cs.st.util.{ Answer, Yes, No, Unknown }

import ObjectType.ArithmeticException

/**
 * Domain to track integer values at a configurable level of precision.
 *
 * @author Riadh Chtara
 * @author Michael Eichberg
 */
trait PreciseIntegerValues[+I]
        extends Domain[I]
        with Configuration
        with IntegerValuesComparison {
    // -----------------------------------------------------------------------------------
    //
    // REPRESENTATION OF INTEGER LIKE VALUES
    //
    // -----------------------------------------------------------------------------------

    /**
     * Determines for a integer value that is updated how large the update can be
     * before we stop the precise tracking of the value and represent the respective
     * value as "some integer value".
     *
     * '''This value is only taken into consideration when two paths converge'''.
     *
     * The default value is 25 which will, e.g., effectively unroll a loop with a loop
     * counter that starts with 0 and which is incremented by one in each round up to
     * 25 times.
     *
     * This is a runtime configurable setting that may affect the overall precision of
     * subsequent analyses that require knowledge about integers.
     */

    protected def spread(a: Int, b: Int): Integer = Math.abs(a - b)

    /**
     * Abstracts over all values with computational type `integer`.
     */
    sealed trait PIntegerValue extends Value { this: DomainValue ⇒

        final def computationalType: ComputationalType = ComputationalTypeInt

    }

    /**
     * Represents a specific, but unknown integer value.
     *
     * Models the top value of this domain's lattice.
     */
    trait AIntegerValue extends PIntegerValue { this: DomainValue ⇒ }
    
    /**
     * Represents a concrete integer value.
     */
    trait IntegerValue extends PIntegerValue { this: DomainValue ⇒
      
        val value: Int
    }

    //
    // QUESTION'S ABOUT VALUES
    //

    def withIntegerValueOrElse[T](value: DomainValue)(f: Int ⇒ T)(orElse: ⇒ T): T =
        value match {
            case v: IntegerValue ⇒ f(v.value)
            case _            ⇒ orElse
        }

    def withIntegerValuesOrElse[T](
        value1: DomainValue,
        value2: DomainValue)(
            f: (Int, Int) ⇒ T)(orElse: ⇒ T): T =
        withIntegerValueOrElse(value1) { v1 ⇒
            withIntegerValueOrElse(value2) { v2 ⇒
                f(v1, v2)
            } {
                orElse
            }
        } {
            orElse
        }

        
    def isEqualToValue(
        value: DomainValue,
        other: Int): Boolean =
        withIntegerValueOrElse(value) {v ⇒
    		(v == other)
        } {
            false
        }

    override def isSomeValueInRange(
        value: DomainValue,
        lowerBound: Int,
        upperBound: Int): Answer =
        withIntegerValueOrElse(value) {
            v ⇒ Answer(lowerBound <= v && v <= upperBound)
        } {
            Answer(true)
        }
        
    override def isSomeValueInRange(
        value: DomainValue,
        lowerBound: DomainValue,
        upperBound: DomainValue): Answer = {

        (value, lowerBound, upperBound) match {
            case (v : IntegerValue, l : IntegerValue, u :  IntegerValue) ⇒
                Answer(l.value <= v.value && v.value <= u.value)
            case (v : IntegerValue, l : IntegerValue, _) if v.value <= l.value ⇒
                if (v == l) Yes else No
            case (v :IntegerValue, _, u : IntegerValue) if v.value >= u.value ⇒
                if (v.value == u.value) Yes else No
            case _ ⇒ Unknown
        }
    }
    
    override def isSomeValueNotInRange(
        value: DomainValue,
        lowerBound: Int,
        upperBound: Int): Answer =
        withIntegerValueOrElse(value) { v ⇒
            Answer(v < lowerBound || v > upperBound)
        } {
            Answer(!(lowerBound == Int.MinValue && upperBound == Int.MaxValue))
        }
        
    override def areEqual(value1: DomainValue, value2: DomainValue): Answer =
        withIntegerValuesOrElse(value1, value2) { (v1, v2) ⇒ Answer(v1 == v2) } { Unknown }
        
    override def isLessThan(
        smallerValue: DomainValue,
        largerValue: DomainValue): Answer =
        withIntegerValuesOrElse(smallerValue, largerValue) { (v1, v2) ⇒
            Answer(v1 < v2)
        } {
            Unknown
        }

    override def isLessThanOrEqualTo(
        smallerOrEqualValue: DomainValue,
        equalOrLargerValue: DomainValue): Answer =
        withIntegerValuesOrElse(smallerOrEqualValue, equalOrLargerValue) { (v1, v2) ⇒
            Answer(v1 <= v2)
        } {
            Unknown
        }

    def updateValueInteger(
        oldValue: DomainValue,
        newValue: DomainValue,
        operands: Operands,
        locals: Locals): (Operands, Locals) =
        (
            operands.map { operand ⇒ if (operand eq oldValue) newValue else operand },
            locals.map { local ⇒ if (local eq oldValue) newValue else local }
        )
        
    def isAIntegerValue(value: DomainValue): Boolean =
        value match {
            case v: AIntegerValue ⇒ true
            case _            ⇒ false
        }

    // -----------------------------------------------------------------------------------
    //
    // HANDLING OF COMPUTATIONS
    //
    // -----------------------------------------------------------------------------------


    //
    // UNARY EXPRESSIONS
    //

    override def ineg(pc: PC, value: DomainValue) =
        withIntegerValueOrElse(value) { v ⇒
            IntegerValue(pc, -v)
        } {
            IntegerValue(pc)
        }

    //
    // BINARY EXPRESSIONS
    //

    override def idiv(
        pc: PC,
        value1: DomainValue,
        value2: DomainValue): Computation[DomainValue, DomainValue] =
        withIntegerValuesOrElse(value1, value2) { (v1, v2) ⇒
            if (v2 == 0)
                ThrowsException(InitializedObjectValue(pc, ArithmeticException))
            else
                ComputedValue(IntegerValue(pc, v1 / v2))
        } {
            if (throwArithmeticExceptions)
                ComputedValueAndException(
                    IntegerValue(pc),
                    InitializedObjectValue(pc, ArithmeticException))
            else
                ComputedValue(IntegerValue(pc))
        }

    override def iadd(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        withIntegerValuesOrElse(value1, value2) { (v1, v2) ⇒
            IntegerValue(pc, v1 + v2)
        } {
            IntegerValue(pc)
        }

    override def iand(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue = {
    	if (isEqualToValue(value1, 0) || isEqualToValue(value2, 0))
    		IntegerValue(pc, 0)
    	else
	        withIntegerValuesOrElse(value1, value2) { (v1, v2) ⇒
	            IntegerValue(pc, v1 & v2)
	        } {
	            IntegerValue(pc)
	        }
    	}

    override def imul(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue = {
    	if (isEqualToValue(value1, 0) || isEqualToValue(value2, 0))
    		IntegerValue(pc, 0)
    	else
	        withIntegerValuesOrElse(value1, value2) { (v1, v2) ⇒
	            IntegerValue(pc, v1 * v2)
	        } {
	            IntegerValue(pc)
	        }
    	}

    override def ior(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        withIntegerValuesOrElse(value1, value2) { (v1, v2) ⇒
            IntegerValue(pc, v1 | v2)
        } {
            IntegerValue(pc)
        }
    //throws ar exception
    override def irem(
        pc: PC,
        value1: DomainValue,
        value2: DomainValue): IntegerLikeValueOrArithmeticException =
        withIntegerValuesOrElse(value1, value2) { (v1, v2) ⇒
            if (v2 == 0l)
                ThrowsException(InitializedObjectValue(pc, ArithmeticException))
            else
                ComputedValue(IntegerValue(pc, v1 % v2))
        } {
            if (throwArithmeticExceptions)
                ComputedValueAndException(
                    IntegerValue(pc),
                    InitializedObjectValue(pc, ArithmeticException))
            else
                ComputedValue(IntegerValue(pc))
        }

    override def ishl(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        withIntegerValuesOrElse(value1, value2) { (v1, v2) ⇒
            IntegerValue(pc, v1 << v2)
        } {
            IntegerValue(pc)
        }

    override def ishr(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        withIntegerValuesOrElse(value1, value2) { (v1, v2) ⇒
            IntegerValue(pc, v1 >> v2)
        } {
            IntegerValue(pc)
        }

    override def isub(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        withIntegerValuesOrElse(value1, value2) { (v1, v2) ⇒
            IntegerValue(pc, v1 - v2)
        } {
            IntegerValue(pc)
        }

    override def iushr(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        withIntegerValuesOrElse(value1, value2) { (v1, v2) ⇒
            IntegerValue(pc, v1 >>> v2)
        }(IntegerValue(pc))

    override def ixor(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        withIntegerValuesOrElse(value1, value2) { (v1, v2) ⇒
            IntegerValue(pc, v1 ^ v2)
        } {
            IntegerValue(pc)
        }
       override def iinc(pc: PC, value: DomainValue, increment: Int) =
	        withIntegerValueOrElse(value) { v ⇒
	            IntegerValue(pc, v + increment)
	        } {
	            IntegerValue(pc)
	        }
    //
    // TYPE CONVERSION INSTRUCTIONS
    //
    override def i2b(pc: PC, value: DomainValue): DomainValue =
        withIntegerValueOrElse(value) { v ⇒ ByteValue(pc, v.toByte) } { ByteValue(pc) }
        
    override def i2c(pc: PC, value: DomainValue): DomainValue =
        withIntegerValueOrElse(value) { v ⇒ CharValue(pc, v.toChar) } { CharValue(pc) }
        
   override def i2s(pc: PC, value: DomainValue): DomainValue =
        withIntegerValueOrElse(value) { v ⇒ StringValue(pc, v.toString) } { DoubleValue(pc) }

    override def i2d(pc: PC, value: DomainValue): DomainValue =
        withIntegerValueOrElse(value) { v ⇒ DoubleValue(pc, v.toDouble) } { DoubleValue(pc) }

    override def i2f(pc: PC, value: DomainValue): DomainValue =
        withIntegerValueOrElse(value) { v ⇒ FloatValue(pc, v.toFloat) } { FloatValue(pc) }

    override def i2l(pc: PC, value: DomainValue): DomainValue =
        withIntegerValueOrElse(value) { v ⇒ LongValue(pc, v.toInt) } { IntegerValue(pc) }

}


