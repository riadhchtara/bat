/* License (BSD Style License):
 * Copyright (c) 2009 - 2013
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tud.cs.st
package bat
package resolved
package ai
package project

/**
 * Encapsulates an exception that is thrown during the creation of the call graph.
 *
 * In general, we do not abort the construction of the overall call graph if an exception
 * is thrown.
 *
 * @author Michael Eichberg
 */
case class CallGraphConstructionException(
        classFile: ClassFile,
        method: Method,
        cause: Exception) {

    import Console._

    override def toString: String = {
        val realCause =
            cause match {
                case ife: InterpretationFailedException[_] ⇒ ife.cause
                case _                                     ⇒ cause
            }

        val stacktrace = realCause.getStackTrace()
        val stacktraceExcerpt =
            if (stacktrace != null && stacktrace.size > 0) {
                val top = stacktrace(0)
                Some(top.getFileName()+"["+top.getClassName()+":"+top.getLineNumber()+"]")
            } else
                None
        classFile.thisType.toJava+"{ "+
            method.toJava+" ⚡ "+
            RED +
            realCause.getClass().getSimpleName()+": "+
            realCause.getMessage() +
            stacktraceExcerpt.map(": "+_).getOrElse("") +
            RESET+
            " }"
    }
}

