/* License (BSD Style License):
 * Copyright (c) 2009 - 2013
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tud.cs.st
package bat
package resolved
package ai
package domain
package l0

import de.tud.cs.st.util.{ Answer, Yes, No, Unknown }

import ObjectType.ArithmeticException

/**
 * Implements the foundations for performing computations related to the long values.
 *
 * @author Riadh Chtara
 * @author Michael Eichberg
 */
trait TypeLevelLongValues[+I]
        extends Domain[I]
        with Configuration {
    domain: Configuration with IntegerValuesComparison with ClassHierarchy ⇒

    // -----------------------------------------------------------------------------------
    //
    // REPRESENTATION OF LONG VALUES
    //
    // -----------------------------------------------------------------------------------

    protected trait LongVal
            extends Value
            with IsLongValue { this: DomainLongValue ⇒

        override final def computationalType: ComputationalType = ComputationalTypeLong
        override def summarize(pc: PC): DomainValue = this

        override def adapt[ThatI >: I](
            targetDomain: Domain[ThatI],
            pc: PC): targetDomain.DomainValue =
            targetDomain.LongValue(pc)
    }

    type DomainLongValue <: LongVal with DomainValue

    /*type DomainKnownLongValue <: LongVal with DomainLongValue
    type DomainUnknownLongValue <: LongVal with DomainLongValue*/

    /**
     * Represents a specific, but unknown long value.
     *
     * Models the top value of this domain's lattice.
     */

    //
    // QUESTION'S ABOUT VALUES
    //

    def withLongValueOrElse[T](value: DomainValue)(f: Long ⇒ T)(orElse: ⇒ T): T =
        orElse

    def withLongValuesOrElse[T](
        value1: DomainValue,
        value2: DomainValue)(
            f: (Long, Long) ⇒ T)(orElse: ⇒ T): T =
        withLongValueOrElse(value1) { v1 ⇒
            withLongValueOrElse(value2) { v2 ⇒
                f(v1, v2)
            } {
                orElse
            }
        } {
            orElse
        }

    def isEqualToLongValue(
        value: DomainValue,
        other: Long): Boolean =
        withLongValueOrElse(value) { v ⇒
            (v == other)
        } {
            false
        }

    // -----------------------------------------------------------------------------------
    //
    // HANDLING OF COMPUTATIONS
    //
    // -----------------------------------------------------------------------------------

    //
    // RELATIONAL OPERATORS
    //

    override def lcmp(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        withLongValuesOrElse(value1, value2) { (v1, v2) ⇒
            if (v1 > v2) IntegerValue(pc, 1)
            else if (v1 == v2) IntegerValue(pc, 0)
            else IntegerValue(pc, -1)
        } {
            IntegerValue(pc)
        }

    //
    // UNARY EXPRESSIONS
    //

    override def lneg(pc: PC, value: DomainValue) =
        withLongValueOrElse(value) { v ⇒
            LongValue(pc, -v)
        } {
            LongValue(pc)
        }

    //
    // BINARY EXPRESSIONS
    //

    def linc(pc: PC, value: DomainValue, increment: Long) =
        withLongValueOrElse(value) { v ⇒
            LongValue(pc, v + increment)
        } {
            LongValue(pc)
        }

    override def ldiv(
        pc: PC,
        value1: DomainValue,
        value2: DomainValue): IntegerLikeValueOrArithmeticException = {
        withLongValuesOrElse(value1, value2) { (v1, v2) ⇒
            if (v2 == 0)
                ThrowsException(InitializedObjectValue(pc, ArithmeticException))
            else
                ComputedValue(LongValue(pc, v1 / v2))
        } {
            if (throwArithmeticExceptions)
                ComputedValueAndException(
                    LongValue(pc),
                    InitializedObjectValue(pc, ArithmeticException))
            else
                ComputedValue(LongValue(pc))
        }
    }

    override def ladd(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        withLongValuesOrElse(value1, value2) { (v1, v2) ⇒
            LongValue(pc, v1 + v2)
        } {
            LongValue(pc)
        }

    override def land(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        if (isEqualToLongValue(value1, 0l) || isEqualToLongValue(value2, 0l))
            LongValue(pc, 0)
        else
            withLongValuesOrElse(value1, value2) { (v1, v2) ⇒
                LongValue(pc, v1 & v2)
            } {
                LongValue(pc)
            }

    override def lmul(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        if (isEqualToLongValue(value1, 0l) || isEqualToLongValue(value2, 0l))
            LongValue(pc, 0)
        else
            withLongValuesOrElse(value1, value2) { (v1, v2) ⇒
                LongValue(pc, v1 * v2)
            } {
                LongValue(pc)
            }

    override def lor(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        withLongValuesOrElse(value1, value2) { (v1, v2) ⇒
            LongValue(pc, v1 | v2)
        } {
            LongValue(pc)
        }

    override def lrem(
        pc: PC,
        value1: DomainValue,
        value2: DomainValue): IntegerLikeValueOrArithmeticException =
        withLongValuesOrElse(value1, value2) { (v1, v2) ⇒
            if (v2 == 0l)
                ThrowsException(InitializedObjectValue(pc, ArithmeticException))
            else
                ComputedValue(LongValue(pc, v1 % v2))
        } {
            if (throwArithmeticExceptions)
                ComputedValueAndException(
                    LongValue(pc),
                    InitializedObjectValue(pc, ArithmeticException))
            else
                ComputedValue(LongValue(pc))
        }

    override def lshl(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        withLongValuesOrElse(value1, value2) { (v1, v2) ⇒
            LongValue(pc, v1 << v2)
        } {
            LongValue(pc)
        }

    override def lshr(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        withLongValuesOrElse(value1, value2) { (v1, v2) ⇒
            LongValue(pc, v1 >> v2)
        } {
            LongValue(pc)
        }

    override def lsub(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        withLongValuesOrElse(value1, value2) { (v1, v2) ⇒
            LongValue(pc, v1 - v2)
        } {
            LongValue(pc)
        }

    override def lushr(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        withLongValuesOrElse(value1, value2) { (v1, v2) ⇒
            LongValue(pc, v1 >>> v2)
        }(LongValue(pc))

    override def lxor(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        withLongValuesOrElse(value1, value2) { (v1, v2) ⇒
            LongValue(pc, v1 ^ v2)
        } {
            LongValue(pc)
        }

    //
    // TYPE CONVERSION INSTRUCTIONS
    //

    override def l2d(pc: PC, value: DomainValue): DomainValue =
        withLongValueOrElse(value) { v ⇒ DoubleValue(pc, v.toDouble) } { DoubleValue(pc) }

    override def l2f(pc: PC, value: DomainValue): DomainValue =
        withLongValueOrElse(value) { v ⇒ FloatValue(pc, v.toFloat) } { FloatValue(pc) }

    override def l2i(pc: PC, value: DomainValue): DomainValue =
        withLongValueOrElse(value) { v ⇒ IntegerValue(pc, v.toInt) } { IntegerValue(pc) }
}
