/* License (BSD Style License):
 * Copyright (c) 2009 - 2013
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tud.cs.st
package bat
package resolved
package ai
package domain
package l0

import de.tud.cs.st.util.{ Answer, Yes, No, Unknown }

/**
 * Implements the foundations for performing computations related to the float values.
 *
 * @author Riadh Chtara
 * @author Michael Eichberg
 */
trait TypeLevelFloatValues[+I]
        extends Domain[I]
        with Configuration {
    domain: Configuration with IntegerValuesComparison with ClassHierarchy ⇒

    // -----------------------------------------------------------------------------------
    //
    // REPRESENTATION OF FLOAT VALUES
    //
    // -----------------------------------------------------------------------------------

    protected trait FloatVal
            extends Value
            with IsFloatValue { this: DomainFloatValue ⇒

        override final def computationalType: ComputationalType = ComputationalTypeFloat
        override def summarize(pc: PC): DomainValue = this

        override def adapt[ThatI >: I](
            targetDomain: Domain[ThatI],
            pc: PC): targetDomain.DomainValue =
            targetDomain.FloatValue(pc)
    }

    type DomainFloatValue <: FloatVal with DomainValue

    //
    // QUESTION'S ABOUT VALUES
    //

    def withFloatValueOrElse[T](value: DomainValue)(f: Float ⇒ T)(orElse: ⇒ T): T =
        orElse

    def withFloatValuesOrElse[T](
        value1: DomainValue,
        value2: DomainValue)(
            f: (Float, Float) ⇒ T)(orElse: ⇒ T): T =
        withFloatValueOrElse(value1) { v1 ⇒
            withFloatValueOrElse(value2) { v2 ⇒
                f(v1, v2)
            } {
                orElse
            }
        } {
            orElse
        }

    def isEqualToFloatValue(
        value: DomainValue,
        other: Float): Boolean =
        withFloatValueOrElse(value) {
            v ⇒ v == other
        } {
            false
        }

    // -----------------------------------------------------------------------------------
    //
    // HANDLING OF COMPUTATIONS
    //
    // -----------------------------------------------------------------------------------

    //
    // RELATIONAL OPERATORS
    //

    override def fcmpg(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue = {
        withFloatValuesOrElse(value1, value2) { (v1, v2) ⇒
            if (v1 == v2)
                IntegerValue(pc, 0)
            else if (v1 < v2 || v1.isNaN || v2.isNaN) IntegerValue(pc, 1)
            else IntegerValue(pc, -1)
        } {
            IntegerValue(pc)
        }
    }

    override def fcmpl(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue = {
        withFloatValuesOrElse(value1, value2) { (v1, v2) ⇒
            if (v1 == v2)
                IntegerValue(pc, 0)
            else if (v1 > v2 || v1.isNaN || v2.isNaN) IntegerValue(pc, 1)
            else IntegerValue(pc, -1)
        } {
            IntegerValue(pc)
        }
    }

    //
    // UNARY EXPRESSIONS
    //

    override def fneg(pc: PC, value: DomainValue) =
        withFloatValueOrElse(value) { v ⇒
            FloatValue(pc, -v)
        } {
            FloatValue(pc)
        }

    //
    // BINARY EXPRESSIONS
    //

    override def fdiv(
        pc: PC,
        value1: DomainValue,
        value2: DomainValue): DomainValue =
        withFloatValuesOrElse(value1, value2) { (v1, v2) ⇒
            FloatValue(pc, v1 / v2)
        } {
            FloatValue(pc)
        }

    override def fadd(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        withFloatValuesOrElse(value1, value2) { (v1, v2) ⇒
            FloatValue(pc, v1 + v2)
        } {
            FloatValue(pc)
        }

    override def fmul(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue = {
        if (isEqualToFloatValue(value1, 0.0f) || isEqualToFloatValue(value2, 0.0f))
            FloatValue(pc, 0.0f)
        else
            withFloatValuesOrElse(value1, value2) { (v1, v2) ⇒
                FloatValue(pc, v1 * v2)
            } {
                FloatValue(pc)
            }
    }

    override def frem(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        withFloatValuesOrElse(value1, value2) { (v1, v2) ⇒
            FloatValue(pc, v1 % v2)
        } {
            FloatValue(pc)
        }

    override def fsub(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        withFloatValuesOrElse(value1, value2) { (v1, v2) ⇒
            FloatValue(pc, v1 - v2)
        } {
            FloatValue(pc)
        }

    //
    // TYPE CONVERSION INSTRUCTIONS
    //

    override def f2d(pc: PC, value: DomainValue): DomainValue =
        withFloatValueOrElse(value) { v ⇒ DoubleValue(pc, v.toDouble) } { DoubleValue(pc) }

    override def f2i(pc: PC, value: DomainValue): DomainValue =
        withFloatValueOrElse(value) { v ⇒ IntegerValue(pc, v.toInt) } { IntegerValue(pc) }

    override def f2l(pc: PC, value: DomainValue): DomainValue =
        withFloatValueOrElse(value) { v ⇒ LongValue(pc, v.toLong) } { LongValue(pc) }

}
