/* License (BSD Style License):
 * Copyright (c) 2009 - 2013
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tud.cs.st
package bat
package resolved
package ai
package domain
package lz

import de.tud.cs.st.util.{ Answer, Yes, No, Unknown }

import ObjectType.ArithmeticException

/**
 * Domain to track float values at a configurable level of precision.
 *
 * @author Riadh Chtara
 * @author Michael Eichberg
 */

trait FloatValues[+I] extends l0.DefaultFloatValuesBinding[I] {
    domain: Configuration with IntegerValuesComparison with ClassHierarchy ⇒

     /**
     * Represents a specific, but unknown float value.
     * we don't need to override doJoin as long as the AFloatValue does represent an
     * arbitrary value
     */
	  case class AFloatV()
	            extends super.AFloatValue {
	  }
	  
     /**
     * Represents a concrete float value.
     */
	  case class FloatVal(
	        override val value: Float)
	            extends super.FloatValue(value) {
	        override def doJoin(pc: PC, other: DomainValue): Update[DomainValue] =
	            other match {
	                case v: FloatVal =>
	                      if (v.value == this.value){
	                          NoUpdate
	                      } else {
	                          StructuralUpdate(other)
	                      }
	                case v: AFloatV =>  StructuralUpdate(other)
	            }  
	        override def toString: String = "FloatValue(value="+value+")"
	  }

	//
	// QUESTION'S ABOUT VALUES
	//

  override def withFloatValueOrElse[T](value: DomainValue)(f: Float ⇒ T)(orElse: ⇒ T): T =
        value match {
            case value : FloatVal ⇒ f(value.value)
            case _     ⇒ orElse
        }
 
}


