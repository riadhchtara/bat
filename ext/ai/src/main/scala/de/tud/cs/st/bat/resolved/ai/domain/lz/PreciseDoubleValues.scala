/* License (BSD Style License):
 * Copyright (c) 2009 - 2013
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  - Neither the name of the Software Technology Group or Technische
 *    Universität Darmstadt nor the names of its contributors may be used to
 *    endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package de.tud.cs.st
package bat
package resolved
package ai
package domain
package lz

import de.tud.cs.st.util.{ Answer, Yes, No, Unknown }

import ObjectType.ArithmeticException

/**
 * Domain to track double values at a configurable level of precision.
 *
 * @author Riadh Chtara
 * @author Michael Eichberg
 */
trait PreciseDoubleValues[+I] extends Domain[I] with Configuration {

    // -----------------------------------------------------------------------------------
    //
    // REPRESENTATION OF DOUBLE LIKE VALUES
    //
    // -----------------------------------------------------------------------------------

    /**
     * Determines for a double value that is updated how large the update can be
     * before we stop the precise tracking of the value and represent the respective
     * value as "some double value".
     *
     * '''This value is only taken into consideration when two paths converge'''.
     *
     * The default value is 25 which will, e.g., effectively unroll a loop with a loop
     * counter that starts with 0 and which is incremented by one in each round up to
     * 25 times.
     *
     * This is a runtime configurable setting that may affect the overall precision of
     * subsequent analyses that require knowledge about doubles.
     */

    protected def spread(a: Double, b: Double): Double = Math.abs(a - b)

    /**
     * Abstracts over all values with computational type `double`.
     */
    sealed trait PDoubleValue extends Value { this: DomainValue ⇒

        final def computationalType: ComputationalType = ComputationalTypeDouble

    }

    /**
     * Represents a specific, but unknown double value.
     *
     * Models the top value of this domain's lattice.
     */
    trait ADoubleValue extends PDoubleValue { this: DomainValue ⇒ }
    
    /**
     * Represents a concrete double value.
     */
    trait DoubleValue extends PDoubleValue { this: DomainValue ⇒
      
        val value: Double
    }

    //
    // QUESTION'S ABOUT VALUES
    //

    def withDoubleValueOrElse[T](value: DomainValue)(f: Double ⇒ T)(orElse: ⇒ T): T =
        value match {
            case v: DoubleValue ⇒ f(v.value)
            case _            ⇒ orElse
        }

    def withDoubleValuesOrElse[T](
        value1: DomainValue,
        value2: DomainValue)(
            f: (Double, Double) ⇒ T)(orElse: ⇒ T): T =
        withDoubleValueOrElse(value1) { v1 ⇒
            withDoubleValueOrElse(value2) { v2 ⇒
                f(v1, v2)
            } {
                orElse
            }
        } {
            orElse
        }

    def isEqualToDoubleValue(
        value: DomainValue,
        other: Double): Boolean =
        withDoubleValueOrElse(value) {
            v ⇒ v == other
        } {
            false
        }

    // -----------------------------------------------------------------------------------
    //
    // HANDLING OF COMPUTATIONS
    //
    // -----------------------------------------------------------------------------------

    //
    // RELATIONAL OPERATORS
    //

    override def dcmpg(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue = {
        withDoubleValuesOrElse(value1, value2) { (v1, v2) ⇒
            if (v1 == v2 || (v1, v2) == (0.0d, -0.0d) || (v1, v2) == (-0.0d, 0.0d))
                IntegerValue(pc, 0)
            else if (v1 < v2 || v1.isNaN || v2.isNaN) IntegerValue(pc, 1)
            else IntegerValue(pc, -1)
        } {
            IntegerValue(pc)
        }
    }
    
    override def dcmpl(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue = {
        withDoubleValuesOrElse(value1, value2) { (v1, v2) ⇒
            if (v1 == v2 || (v1, v2) == (0.0d, -0.0d) || (v1, v2) == (-0.0d, 0.0d))
                IntegerValue(pc, 0)
            else if (v1 > v2 || v1.isNaN || v2.isNaN) IntegerValue(pc, 1)
            else IntegerValue(pc, -1)
        } {
            IntegerValue(pc)
        }
    }
    
    //
    // UNARY EXPRESSIONS
    //

    override def dneg(pc: PC, value: DomainValue) =
        withDoubleValueOrElse(value) { v ⇒
            DoubleValue(pc, -v)
        } {
            DoubleValue(pc)
        }

    //
    // BINARY EXPRESSIONS
    //

    override def ddiv(
        pc: PC,
        value1: DomainValue,
        value2: DomainValue): DomainValue =
        withDoubleValuesOrElse(value1, value2) { (v1, v2) ⇒
            DoubleValue(pc, v1 / v2)
        } {
            DoubleValue(pc)
        }

    override def dadd(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        withDoubleValuesOrElse(value1, value2) { (v1, v2) ⇒
            DoubleValue(pc, v1 + v2)
        } {
            DoubleValue(pc)
        }

    override def dmul(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue = {
    	if (isEqualToDoubleValue(value1, 0.0d) || isEqualToDoubleValue(value2, 0.0d))
    		DoubleValue(pc, 0.0d)
    	else
	        withDoubleValuesOrElse(value1, value2) { (v1, v2) ⇒
	            DoubleValue(pc, v1 * v2)
	        } {
	            DoubleValue(pc)
	        }
    	}


    override def drem(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        withDoubleValuesOrElse(value1, value2) { (v1, v2) ⇒
            DoubleValue(pc, v1 % v2)
        } {
            DoubleValue(pc)
        }

    override def dsub(pc: PC, value1: DomainValue, value2: DomainValue): DomainValue =
        withDoubleValuesOrElse(value1, value2) { (v1, v2) ⇒
            DoubleValue(pc, v1 - v2)
        } {
            DoubleValue(pc)
        }

    //
    // TYPE CONVERSION INSTRUCTIONS
    //

    override def d2f(pc: PC, value: DomainValue): DomainValue =
        withDoubleValueOrElse(value) { v ⇒ FloatValue(pc, v.toFloat) } { FloatValue(pc) }

    override def d2i(pc: PC, value: DomainValue): DomainValue =
        withDoubleValueOrElse(value) { v ⇒ IntegerValue(pc, v.toInt) } { IntegerValue(pc) }

    override def d2l(pc: PC, value: DomainValue): DomainValue =
        withDoubleValueOrElse(value) { v ⇒ LongValue(pc, v.toInt) } { LongValue(pc) }
}


